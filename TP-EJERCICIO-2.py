import os

def pausa ():
    print()
    input('Presione cualquier tecla para poder continuar...')
    os.system('cls')

def menu():
    print('1) Cargar Productos')
    print('2) Mostrar el listado de Producto')
    print('3) Mostrar stock ( desde / hasta )')
    print('4) Aumentar Stock')
    print('5) Eliminar Productos con Stock (cero) ')
    print('6) Salir')
    opcion = int(input('Elija una opción: '))
    while not((opcion >= 1) and (opcion <= 6)):
        opcion = int(input('Elija una opción: '))
    os.system('cls')
    return opcion

def leerCodigo():
    codigo = int(input('Codigo: '))
    while not(codigo >= 0):
        codigo = int(input('Codgio: '))
    return codigo

def leerPrecio():
    precio = int(input('Precio: '))
    while not(precio >= 0):
        precio= int(input('Precio: '))
    return precio

def leerStock():
    stock = int(input('Stock: '))
    while not(stock >= 0):
        stock= int(input('Stock: '))
    return stock
    
def mostrar(diccionario):
    print('Listado Productos')
    for codigo, datos in diccionario.items():
        print(codigo,datos)

def leerProductos():
    print('Cargar Lista de Productos')
    productos = { }
    codigo = -1
    while (codigo != 0):
        codigo = int(input('Codigo (cero para finalizar): '))
        if codigo != 0: 
            if codigo not in productos:    
                descripcion = input('Descripcion: ')
                precio = leerPrecio()
                stock = leerStock()
                productos[codigo] = [descripcion,precio,stock]
                print('agregado correctamente')
            else:
                print('el Producto ya existe')

    return productos

def buscarStock(productos):
    codigo = int(input('codigo: '))
    stock  = productos.get(codigo,-1)
    return codigo,stock


def eliminar(productos):
    print('Eliminar stock cero ')
    codigo, stock = buscarStock(productos)   
    if (stock != -1):
        print('Se va a eliminar: ', productos[codigo])
        confirmacion = input('Confirma eliminarlo s/n: ')
        if (confirmacion == 's'):
            del productos[codigo,stock]
            print ('Eliminado ...')
    else:
        print('El Producto no existe') 

###### Principal ########
opcion = 0

os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = leerProductos()
    elif opcion == 2:
        mostrar(productos)
    elif opcion == 3:
        buscar(productos)
    elif opcion == 4:
        modificar(productos)
    elif opcion == 5:
        eliminar(productos)
    elif opcion == 6:    
        salir()
    pausa()
